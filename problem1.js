let fs = require('fs');
const problem1 = (() => {


    let makeDir = () => {
        let dirPromise = new Promise((resolve, reject) => {
            fs.mkdir('/home/sachin/project/promiseProject/test/hello', (err) => {
                if (!err) {
                    resolve('directory created');
                }
                else {
                    reject(err);
                }
            });

        })
        return dirPromise;
    }




    let random = Math.floor(Math.random() * 10);

    let createdFiles = () => {

        let index = 0;
        let interval = setInterval(() => {
            let writePromise = new Promise((resolve, reject) => {
                fs.writeFile(`/home/sachin/project/promiseProject/test/hello/random${index}.json`, 'Random JSON', (error) => {
                    if (!error) {
                        console.log('file is created successfully.');
                        resolve();
                    } else {
                        reject(error);
                    }

                });

            });

            if (index === random) {
                clearInterval(interval);
            }
            index++;

            return writePromise;
        }, 500);
    }



    let getDeleted = () => {
        let index = 0;
        let interval = setInterval(() => {
            let deletePromise = new Promise((resolve, reject) => {
                fs.unlink(`/home/sachin/project/promiseProject/test/hello/random${index}.json`, (error) => {
                    if (!error) {
                        console.log('file deleted successfully.');
                        resolve();
                    } else {
                        reject(error);
                    }

                });

            });

            if (index === random) {
                clearInterval(interval);
            }
            index++;

            return deletePromise;
        }, 1000);



    }

    makeDir().then((val)=>{
        console.log(val);
        return createdFiles();
    })
    .then(()=>{
       setTimeout(()=>{
        return getDeleted();
       },5000)
    })
    .catch((err)=>{
        console.error(err);
    })



});

module.exports = problem1;