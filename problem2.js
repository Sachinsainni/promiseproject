

const { doesNotReject } = require('assert');
let fs = require('fs');
const { resolve } = require('path');

function problem2(lipsumPath) {
    function readLipsumFile(lipsumPath) {
        let readPromise = new Promise((resolve, reject) => {

            fs.readFile(lipsumPath, 'utf8', (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    console.log("Read lipsumFile.txt")
                    resolve(data);
                }
            })
        })
        return readPromise;
    }


    function writeUpperCase(data) {
        let inUppercase = data.toUpperCase();
        let fileName = 'upperCase.txt';
        let filePath = `/home/sachin/project/promiseProject/data/${fileName}`;
        let upperCasePromise = new Promise((resolve, reject) => {
            fs.writeFile(filePath, inUppercase, (err) => {
                if (err) {
                    reject(err);
                } else {
                    console.log(`File ${fileName} write successfully`);
                    writeFileName(fileName);
                    resolve(inUppercase);
                }
            });
        })
        return upperCasePromise;


    }

    function writeLowerCase(data) {
        let inLowercase = data.toLowerCase()
            .split('.')
            .join('\n');
        let fileName = 'lowerCase.txt';
        let filePath = `/home/sachin/project/promiseProject/data/${fileName}`;
        let lowerCasePromise = new Promise((resolve, reject) => {
            fs.writeFile(filePath, inLowercase, (err) => {
                if (err) {
                    reject(err);
                } else {
                    console.log(`File ${fileName} write successfully`);
                    writeFileName(fileName);
                    resolve(filePath);
                }
            });
        })
        return lowerCasePromise;
    }

    function sortingContent(pathOfFile) {
        let sortingPromise = new Promise((resolve, reject) => {
            fs.readFile(pathOfFile, 'utf8', (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    console.log("read the new file");
                    let dataSorted = data.split(' ')
                        .sort().join(' ');
                    let fileName = "sortedContent.txt";
                    let filePath = `/home/sachin/project/promiseProject/data/${fileName}`;
                    // let sortPromise = new Promise((resolve, reject) => {
                    fs.writeFile(filePath, dataSorted, (err) => {
                        if (err) {
                            reject(err);
                        } else {
                            console.log(`file ${fileName} write successfully.`)
                        }
                    })
                    resolve(fileName)
                }
            })
        })
        return sortingPromise;
    }

  

    function writeFileName(file) {
        let fileName = "fileName.txt";
        let filePath = `/home/sachin/project/promiseProject/data/${fileName}`;
        let writePromise = new Promise((resolve, reject) => {

            fs.appendFile(filePath, ' ' + file, (err) => {
                if (err) {
                    reject(err);
                } else {
                    console.log(`file ${file} written in ${fileName} file`);
                    resolve()
                }
            })
        })
        return writePromise;
    }



    function deleteFile() {
        let fileName = 'fileName.txt'
        let pathOfDir = '/home/sachin/project/promiseProject/data/'
        let filePath = `${pathOfDir}${fileName}`
        let deletePromise = new Promise((resolve, reject) => {
            fs.readFile(filePath, 'utf8', (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    console.log("fileName.txt read successfully");
                    let files = data.trim()
                        .split(' ');
                    let deletedFile = files.map((item) => {
                        fs.unlink(`${pathOfDir}${item}`, (err) => {
                            if (err) {
                                reject(err);
                            } else {
                                console.log(`${item} file deleted successfully`);
                                resolve();
                            }
                            return item;
                        })
                    })
                    resolve();
                }
            })
        })
        return deletePromise;
    }

    readLipsumFile(lipsumPath).then((data) => {
        return writeUpperCase(data);
    }).then((writeUpperCase) => {
        return writeLowerCase(writeUpperCase);
    }).then((writeLowerCase) => {
        return sortingContent(writeLowerCase);
    }).then((sortingContent) => {
        return writeFileName(sortingContent);
    }).then(() => {
        return deleteFile();
    }).catch((err) => {
        console.log(err);
    })
}




module.exports = problem2;